const database = require('./setup/database');
const app = require('./setup/app');

database.connect();
app.initialize();
